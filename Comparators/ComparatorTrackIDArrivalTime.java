package DiskSchedulingAlgorithm.Comparators;

import java.util.Comparator;

import DiskSchedulingAlgorithm.models.QuerySimulated;

public class ComparatorTrackIDArrivalTime implements Comparator<QuerySimulated>{
	int head;
	public ComparatorTrackIDArrivalTime(int head) {
		this.head = head;
	}
    @Override
    public int compare(QuerySimulated o1, QuerySimulated o2) {
    	
        int result = o1.getArrivalTime() - o2.getArrivalTime();
        if(result != 0)
        	return result;
    	return Math.abs(o1.getTrackID() - head) - Math.abs(o2.getTrackID() - head);
    }
}
