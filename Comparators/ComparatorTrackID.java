package DiskSchedulingAlgorithm.Comparators;

import java.util.Comparator;

import DiskSchedulingAlgorithm.models.QuerySimulated;

public class ComparatorTrackID  implements Comparator<QuerySimulated>{
	
	    @Override
	    public int compare(QuerySimulated o1, QuerySimulated o2) {
	    	int result = o1.getArrivalTime() - o2.getArrivalTime();
	    	if(result == 0) {
	    		return Math.abs(o1.getTrackID()) - Math.abs(o2.getTrackID());
	    	}
	    	return result;
	    }
}
