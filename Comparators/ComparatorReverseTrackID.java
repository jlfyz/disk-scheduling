package DiskSchedulingAlgorithm.Comparators;

import java.util.Comparator;

import DiskSchedulingAlgorithm.models.QuerySimulated;

public class ComparatorReverseTrackID  implements Comparator<QuerySimulated>{
	
    @Override
    public int compare(QuerySimulated o2, QuerySimulated o1) {
    	int result = o2.getArrivalTime() - o1.getArrivalTime();
    	if(result == 0) {
    		return Math.abs(o2.getTrackID()) - Math.abs(o1.getTrackID());
    	}
    	return result;
    }
}
