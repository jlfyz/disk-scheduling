package DiskSchedulingAlgorithm.Comparators;

import java.util.Comparator;

import DiskSchedulingAlgorithm.models.QuerySimulated;

public class ComparatorArrivalTime implements Comparator<QuerySimulated>{
    @Override
    public int compare(QuerySimulated o1, QuerySimulated o2) {
    	return o1.getArrivalTime() - o2.getArrivalTime();
    }
}
