package DiskSchedulingAlgorithm.managers;

import java.util.ArrayList;
import java.util.Collections;

import DiskSchedulingAlgorithm.Comparators.ComparatorArrivalTime;
import DiskSchedulingAlgorithm.models.QuerySimulated;

public class FCFSDiskManager extends DiskManager {
	//Arrlist queries
	public FCFSDiskManager (ArrayList<QuerySimulated> queries, int head) {
		super(queries, head);
		Collections.sort(this.queries, new ComparatorArrivalTime());
	}

	@Override
	public String algoName() {
		return "FCFS";
	}
	
	@Override
	public Long estimateExecutionTime() {
		Long answer = 0L;
		for (QuerySimulated querySimulated : this.queries) {
			checkDeadline(answer, querySimulated);
			answer += Math.abs(head - querySimulated.getTrackID());
			
			updateHead(querySimulated.getTrackID());
		}
		return answer;
	}
	
	@Override
	public String toString() {
		String res = "\"processManagerFIFO\": {\"algorithmName\": \"First in first out\", \"estimateExecutionTime\": "+ this.estimateExecutionTime()+ ", \"missedDeadlines\": "+ this.missedDeadlines + "}";
		return res;
	}
}
