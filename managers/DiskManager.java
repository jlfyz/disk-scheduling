package DiskSchedulingAlgorithm.managers;

import java.util.ArrayList;

import DiskSchedulingAlgorithm.models.QuerySimulated;


public class DiskManager {
	protected int missedDeadlines;
	ArrayList<QuerySimulated> queries = new ArrayList<QuerySimulated>();
	protected int head;
	
	public DiskManager(ArrayList<QuerySimulated> queries, int head) {
		this.queries = new ArrayList<QuerySimulated>(queries);
		this.head = head;
	}
	
	public String algoName() {
		return "";
	}
	
	public void clearQueries() {
		for(QuerySimulated q: queries) {
			q.setProcessed(false);
		}
	}
	
	public void add(QuerySimulated query) {
		this.queries.add(query);
	}
	
	public void updateHead(int newHead) {
		this.head = newHead;
	}
	
	public void checkDeadline(long time, QuerySimulated query) {
		if(query.getArrivalTime() + query.getDeadline() < time) {
			incMissedDeadlines();
		}
	}
	
	private void incMissedDeadlines() {
		this.missedDeadlines++;
	}
	
	public Long estimateExecutionTime() {
		return 0L;
	}
}
