package DiskSchedulingAlgorithm.managers;

import java.util.ArrayList;
import java.util.Collections;

import DiskSchedulingAlgorithm.Comparators.ComparatorReverseTrackID;
import DiskSchedulingAlgorithm.Comparators.ComparatorTrackID;
import DiskSchedulingAlgorithm.models.QuerySimulated;

public class ScanDiskManager extends DiskManager{

	private int initialTime;
	public ScanDiskManager(ArrayList<QuerySimulated> queries, int head) {
		super(queries, head);
		Collections.sort(this.queries, new ComparatorTrackID());
		this.initialTime = 0;
	}
	
	public ScanDiskManager(ArrayList<QuerySimulated> queries, int head, int time) {
		super(queries, head);
		Collections.sort(this.queries, new ComparatorTrackID());
		this.initialTime = time;
	}

	@Override
	public String algoName() {
		return "C-Scan";
	}
	
	@Override
	public Long estimateExecutionTime() {
		Long answer = 0L;
		int proccesedCounter = 0;
		Long time = (long) this.initialTime;
		int i = 0; 
		while(proccesedCounter < this.queries.size() - 1) {
			for (QuerySimulated querySimulated : this.queries) {
				if(querySimulated.isProcessed()) 
					continue;
				if(querySimulated.getArrivalTime() <= time) {
					answer += Math.abs(head-querySimulated.getTrackID());
					checkDeadline(time, querySimulated);
					time = answer;
					updateList(querySimulated);
					
					proccesedCounter++;
					
				}
				else {
					time++;
				}
				if (proccesedCounter == this.queries.size() - 1){
					 break;
				}
			}
			if(i % 2 == 0) {
				Collections.sort(this.queries, new ComparatorReverseTrackID());
			}
			else {
				Collections.sort(this.queries, new ComparatorTrackID());
			}
			i++;
		}
		clearQueries();
		return answer;
	}

	private void updateList(QuerySimulated querySimulated) {
		updateHead(querySimulated.getTrackID());
		querySimulated.setProcessed(true);
	}
	
	@Override
	public String toString() {
		String res = "\"processManagerScan\": {\"algorithmName\": \"Scan\", \"estimateExecutionTime\": "+ this.estimateExecutionTime() + ", \"missedDeadlines\": "+ this.missedDeadlines + "}";
		return res;
	}
}
