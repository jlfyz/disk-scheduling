package DiskSchedulingAlgorithm.managers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import DiskSchedulingAlgorithm.Comparators.ComparatorTrackIDArrivalTime;
import DiskSchedulingAlgorithm.models.QuerySimulated;

public class SSTFDiskManager extends DiskManager {

	public SSTFDiskManager(ArrayList<QuerySimulated> queries, int head) {
		super(queries, head);
		Collections.sort(this.queries, new ComparatorTrackIDArrivalTime(head));
	}
	
	@Override
	public String algoName() {
		return "SSTF";
	}
	
	@Override
	public Long estimateExecutionTime() {
		Long answer = 0L;
		long time = 0;
		int processCount = 0;
		for(;processCount != this.queries.size() - 1; time++) {
			ArrayList<QuerySimulated> tmp = new ArrayList<QuerySimulated>(getQueriesWithArrivalTime(time, head));
			if(tmp.size() == 0) {
				continue;
			}
			checkDeadline(time, tmp.get(0));
			answer += Math.abs(head - tmp.get(0).getTrackID());
			
			updateList(tmp);
			processCount++;
		}
		clearQueries();
		return answer;
	}

	private void updateList(ArrayList<QuerySimulated> tmp) {
		updateHead(tmp.get(0).getTrackID());
		tmp.get(0).setProcessed(true);
		tmp.clear();
	}
	
	private List<QuerySimulated> getQueriesWithArrivalTime(long time, int head) {
		return queries.stream()
					  .filter(o -> o.getArrivalTime() <= time && !o.isProcessed())
					  .sorted(new ComparatorTrackIDArrivalTime(head))
					  .toList();
	}
	
	@Override
	public String toString() {
		String res = "\"processManagerSSTF\": {\"algorithmName\": \"Shortest Seek Time First\", \"estimateExecutionTime\": "+ this.estimateExecutionTime()+ ", \"missedDeadlines\": "+ this.missedDeadlines + "}";
		return res;
	}
}
