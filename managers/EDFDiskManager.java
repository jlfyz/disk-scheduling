package DiskSchedulingAlgorithm.managers;

import java.util.ArrayList;
import java.util.Collections;

import DiskSchedulingAlgorithm.Comparators.ComparatorArrivalTimeAndDeadline;
import DiskSchedulingAlgorithm.models.QuerySimulated;

public class EDFDiskManager extends DiskManager {
	public EDFDiskManager(ArrayList<QuerySimulated> queries, int head) {
		super(queries, head);
		Collections.sort(this.queries, new ComparatorArrivalTimeAndDeadline());
		this.missedDeadlines = 0;
	}
	
	@Override
	public String algoName() {
		return "EDF";
	}
	
	@Override
	public Long estimateExecutionTime() {
		Long answer = 0L;
		int processed = 0;
		int time = 0;
		while(processed != this.queries.size() - 1) {
			for(QuerySimulated query: this.queries) {
				if (time == query.getArrivalTime()) {
					checkDeadline(time, query);
					answer += Math.abs(head - query.getTrackID());
					
					updateList(query);
					processed++;
				}
				if(processed == this.queries.size() - 1)
					break;
			}
			time++;
		}
		clearQueries();
		return answer;
	}

	private void updateList(QuerySimulated query) {
		updateHead(query.getTrackID());
		query.setProcessed(true);
	}
	
	@Override
	public String toString() {
		String res = "\"processManagerEDF\": {\"algorithmName\": \"Earliest Deadline First\", \"estimateExecutionTime\": "+ this.estimateExecutionTime() + ", \"missedDeadlines\": "+ this.missedDeadlines + "}";
		return res;
	}
}
