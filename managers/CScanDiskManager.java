package DiskSchedulingAlgorithm.managers;

import java.util.ArrayList;
import java.util.Collections;

import DiskSchedulingAlgorithm.Comparators.ComparatorTrackID;
import DiskSchedulingAlgorithm.models.QuerySimulated;

public class CScanDiskManager extends DiskManager {
	private int initialTime;
	
	public CScanDiskManager (ArrayList<QuerySimulated> queries, int head) {
		super(queries, head);
		Collections.sort(this.queries, new ComparatorTrackID());
		this.initialTime = 0;
	}

	public CScanDiskManager (ArrayList<QuerySimulated> queries, int head, int time) {
		super(queries, head);
		Collections.sort(this.queries, new ComparatorTrackID());
		this.initialTime = time;
	}
	
	@Override
	public String algoName() {
		return "C-Scan";
	}
	
	@Override
	public Long estimateExecutionTime() {
		Long answer = 0L;
		int proccesedCounter = 0;
		Long time = (long) this.initialTime;
		int lastQuery = this.queries.get(this.queries.size()-1).getTrackID();
		int difference = 0;
		while(proccesedCounter < this.queries.size() - 1) {
			answer += difference;
			time += difference;
					
			for (QuerySimulated querySimulated : this.queries) {
				if(querySimulated.isProcessed()) 
					continue;
				if(querySimulated.getArrivalTime() <= time) {
					checkDeadline(time, querySimulated);
					
					answer += Math.abs(head - querySimulated.getTrackID());
					time = answer;
					
					difference = lastQuery - querySimulated.getTrackID();
					
					updateList(querySimulated);
					
					proccesedCounter++;

				}
				else {
					time++;
				}
				if (proccesedCounter == this.queries.size() - 1){
					 break;
				}
			}
			updateHead(0);
		}
		clearQueries();
		return answer;
	}

	private void updateList(QuerySimulated querySimulated) {
		updateHead(querySimulated.getTrackID());
		querySimulated.setProcessed(true);
	}
	
	@Override
	public String toString() {
		String res = "\"processManagerC-Scan\": {\"algorithmName\": \"C-Scan\", \"estimateExecutionTime\": "+ this.estimateExecutionTime()+ ", \"missedDeadlines\": "+ this.missedDeadlines + "}";
		return res;
	}
}
