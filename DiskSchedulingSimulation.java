package DiskSchedulingAlgorithm;

import java.util.ArrayList;

import DiskSchedulingAlgorithm.managers.CScanDiskManager;
import DiskSchedulingAlgorithm.managers.DiskManager;
import DiskSchedulingAlgorithm.managers.EDFDiskManager;
import DiskSchedulingAlgorithm.managers.FCFSDiskManager;
import DiskSchedulingAlgorithm.managers.SSTFDiskManager;
import DiskSchedulingAlgorithm.managers.ScanDiskManager;
import DiskSchedulingAlgorithm.models.QuerySimulated;

public class DiskSchedulingSimulation {
	private static ArrayList<QuerySimulated> queries = new ArrayList<QuerySimulated>();
	
	public static void main(String[] args) {
		DiskManager[] diskManager = new DiskManager[5];
		createArrayList(50, 1, 2, 2);//queries, dealinde coef, arrival time coef, deadline multiplier
		System.out.println("{\"queue\": " + queries.toString() + ",");
		
		int head = 1 + (int) (Math.random() * 20 * 2);
		 
		diskManager[0] = new FCFSDiskManager(queries, head);
		diskManager[1] = new SSTFDiskManager(queries, head);
		diskManager[2] = new ScanDiskManager(queries, head, 50);
		diskManager[3] = new CScanDiskManager(queries, head, 50);
		diskManager[4] = new EDFDiskManager(queries, head);
		
		System.out.println(diskManager[0] + ",");
		System.out.println(diskManager[1] + ",");
		System.out.println(diskManager[2] + ",");
		System.out.println(diskManager[3] + ",");
		System.out.println(diskManager[4] + "}");
			
	}
	
	public static void createArrayList(int queriesCount, int deadlineCoeff, int arrivalTimeCoefficient, int deadlineMultiplier) {
		for (int i = 0; i < queriesCount; i++) {
			queries.add(new QuerySimulated(
					i + 1 + (int)(Math.random() * queriesCount + i),
					1 + (int) (Math.random() * queriesCount / deadlineCoeff * deadlineMultiplier),
					i + 1 + (int) (Math.random() * queriesCount / arrivalTimeCoefficient)));
			}
	}
}
