package DiskSchedulingAlgorithm.models;

public class QuerySimulated {
	private int arrivalTime;
	private int deadline;
	private int trackID;
	public static int head;
	public boolean isProcessed;
	
	public QuerySimulated(int trackId, int deadline, int arTime) {
		this.arrivalTime = arTime;
		this.deadline = deadline;
		this.trackID = trackId;
		this.isProcessed = false;
	}
	
	public boolean isProcessed() {
		return isProcessed;
	}
	public void setProcessed(boolean p) {
		isProcessed = p;
	}
	
	public int getArrivalTime() {
		return arrivalTime;
	}
	public void setArrivalTime(int arrivalTime) {
		this.arrivalTime = arrivalTime;
	}
	public int getDeadline() {
		return deadline;
	}
	public void setDeadline(int deadline) {
		this.deadline = deadline;
	}
	public int getTrackID() {
		return trackID;
	}
	public void setTrackID(int trackID) {
		this.trackID = trackID;
	}
	public String toString() {
    	String result = "{"
    			+ "\"trackID\": " + getTrackID()+", "
    			+ "\"arrivalTime\": " + getArrivalTime()+", "
    		    + "\"isProcessed\": \"" + isProcessed()+"\", "
    			+ "\"deadline\": " + getDeadline()+"}";
    	return result;
    }
}
